# LABORATORY DUAL SWITCIHNG POWER SUPPLY

The purpose of this page is to explain step by step the realization of a dual laboratory power supply delivering 2x5V or 2x12V.

It is of course protected against short circuits.

In addition to the ON / OFF switch and its blue LED it has a switch to enable / disable the output. A red LED indicates that the output is active.

The power supply uses the following components :

 * 2 switching modules
 * some passive components

The schematics include two output switching boards :
 * a 2x12V board
 * a 5V + 3.3V board

### ELECTRONICS

The schema is made using KICAD.

### BLOG
A description in french here : https://riton-duino.blogspot.com/2019/01/alimentation-de-labo-decoupage.html